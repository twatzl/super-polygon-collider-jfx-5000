package main;

/**
 * Created by tobias on 12.02.14.
 */
public class Utility {

    public static double max(double[] arr){
        double max = arr[0];
        for (double d : arr){
            if (d > max)
                max = d;
        }
        return max;
    }

}
