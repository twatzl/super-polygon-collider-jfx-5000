package main;

import java.util.Random;

/**
 * Created by tobias on 12.02.14.
 */
public class Polygons {

    private static boolean AreParallel(Vector v1, Vector v2){
        return (v1.getDeltaX()*v2.getDeltaY() - v1.getDeltaY()*v2.getDeltaX() == 0);
    }

    private static Point Distance(Point p1, Vector v1, Point p2){
        double distX;
        double distY;

        if (v1.getDeltaX() == 0){
            distX = p2.getX() - p1.getX();
        } else {
            distX = (p2.getX() - p1.getX()) / v1.getDeltaX();
        }

        if (v1.getDeltaY() == 0){
            distY = p2.getY() - p1.getY();
        } else {
            distY = (p2.getY() - p1.getY()) / v1.getDeltaY();
        }

        return new Point(distX,distY);
    }

    public static boolean DoVectorsIntersect(Vector v1, Vector v2){

        if (AreParallel(v1, v2)){
            double distStart = Distance(v1.getStart(), v1, v2.getStart()).getX();
            double distEnd = Distance(v1.getStart(), v1, v2.getEnd()).getX();

            if (Math.round(distStart*10000) == Math.round(distEnd*10000)){
                boolean a = (distStart < 1 && distStart > 0 && (distEnd < 0 || distEnd > 1));
                boolean b = (distEnd < 1 && distEnd > 0 && (distStart < 0 || distStart > 1));
                boolean c = (distStart > 0 && distEnd > 0 && distStart < 1 && distEnd < 1);
                boolean d = ((distStart < 0 && distEnd > 1)||(distStart > 1 && distEnd < 0));
                if (a || b || c || d){
                    return true;
                } else {
                    return false;
                }
            }
        }

        Point p1 = v1.getStart();
        Point p2 = v1.getEnd();
        Point p3 = v2.getStart();
        Point p4 = v2.getEnd();

        //double s = (v1.getStart().getX() * v2.getDeltaY() - v2.getStart().getX() * v2.getDeltaY() + v2.getStart().getY() * v2.getDeltaX() - v1.getDeltaY() *v2.getDeltaX()) / (v1.getDeltaY() * v2.getDeltaX() - v1.getDeltaX() * v2.getDeltaY());
        //double t = (v1.getStart().getX() + s * v1.getDeltaX() - v2.getStart().getX()) / v2.getDeltaX();

        double t = (v2.getDeltaY()*(p1.getX()-p3.getX())-v2.getDeltaX()*p1.getY()+p3.getY()*v2.getDeltaX())/(v1.getDeltaY()*v2.getDeltaX() - v1.getDeltaX()*v2.getDeltaY());

        Point p = new Point(v1.getStart().getX() + t * v1.getDeltaX(), v1.getStart().getY() + t * v1.getDeltaY());

        Point distV1 = Distance(v1.getStart(), v1, p);
        Point distV2 = Distance(v2.getStart(), v2, p);

         boolean returnVal = (distV1.getX() >= 0 && distV1.getX() <= 1
                && distV1.getY() >= 0 && distV1.getY() <= 1
                && distV2.getX() >= 0 && distV2.getX() <= 1
                && distV2.getY() >= 0 && distV2.getY() <= 1);
        return returnVal;

        /*
        p1[[x]] + ss*v1[[x]] == p3[[x]] + tt*v2[[x]]
        p1x + ss * v1x - p3x = tt*v2x

        (p1x + ss * v1x - p3x) / v2x = tt


        p1[[y]] + ss*v1[[y]] == p3[[y]] + tt*v2[[y]]

        (p1y + ss * v1y - p3y) / v2y = tt

        (p1x + ss * v1x - p3x) / v2x = (p1y + ss * v1y - p3y) / v2y
        (p1x + ss * v1x - p3x) * v2y = (p1y + ss * v1y - p3y) * v2x

        p1x * v2y + ss * v1x * v2y - p3x * v2y
        = p1y * v2x + ss * v1y * v2x - p3y * v2x

        p1x * v2y - p3x * v2y + p3y * v2x - ply *v2x = ss * vly * v2x - ss * vlx * v2y

        p1x * v2y - p3x * v2y + p3y * v2x - ply *v2x = ss * (vly * v2x - vlx * v2y)

        ss = (p1x * v2y - p3x * v2y + p3y * v2x - ply *v2x) / (vly * v2x - vlx * v2y)

        */

    }

    public static boolean DoPolygonsIntersect(Polygon p1, Polygon p2){
        double [][] points1 = p1.getPoints();
        double [][] points2 = p2.getPoints();
        int lastIndex1;
        int lastIndex2;

        /*for (int i = 0; i < p1.size(); i++){
            if (IsPointInPolygon(p2, new Point(points1[Polygon.X][i], points1[Polygon.Y][i]))){
                return true;
            }
        }
        for (int i = 0; i < p2.size(); i++){
            if (IsPointInPolygon(p1, new Point(points2[Polygon.X][i], points2[Polygon.Y][i]))){
                return true;
            }
        }*/

        lastIndex1 = p1.size() - 1;
        lastIndex2 = p2.size() - 1;

        for (int i = 0; i < p1.size(); i++){
            for (int j = 0; j < p2.size(); j++){
                if (DoVectorsIntersect(
                        new Vector(new Point(points1[Polygon.X][lastIndex1], points1[Polygon.Y][lastIndex1]),
                                new Point(points1[Polygon.X][i], points1[Polygon.Y][i])),
                        new Vector(new Point(points2[Polygon.X][lastIndex2], points2[Polygon.Y][lastIndex2]),
                                new Point(points2[Polygon.X][j], points2[Polygon.Y][j])))){
                    return true;
                }
                lastIndex2 = j;
            }
            lastIndex1 = i;
        }

        return false;
    }

    public static boolean IsPointInPolygon(Polygon polygon, Point point){
        int intersects = CountIntersect(polygon, point);
        return !(intersects % 2 == 0);
    }

    private static int CountIntersect(Polygon polygon, Point point){
        int intersectCounter = 0;
        int lastIndex = polygon.size() - 1; // We have to also watch the last vector; so we start with the last one
        double [] polygonXPoints = polygon.getXPoints();
        double [] polygonYPoints = polygon.getYPoints();
        Point randomPoint = RandomPoint(Utility.max(polygon.getXPoints()), Utility.max(polygon.getYPoints()));
        Vector intersectVector;

        while (CutsVertex(polygon, randomPoint, point)){
            randomPoint = RandomPoint(Utility.max(polygon.getXPoints()), Utility.max(polygon.getYPoints()));
        }

        intersectVector = new Vector(randomPoint, point);

        for (int i = 0; i < polygon.size(); i++){
            if (DoVectorsIntersect(intersectVector, new Vector(new Point(polygonXPoints[lastIndex], polygonYPoints[lastIndex]), new Point(polygonXPoints[i], polygonYPoints[i])))){
                intersectCounter++;
            }
            lastIndex = i;
        }

        return intersectCounter;
    }

    private static Point RandomPoint(double minX, double minY){
        Random random = new Random();
        return new Point(minX + 1 + random.nextInt(10000), minY + 1 + random.nextInt(10000));
    }

    private static boolean CutsVertex(Polygon polygon, Point randomPoint, Point point){
        boolean cutsVertex = false;
        double [] polygonXPoints = polygon.getXPoints();
        double [] polygonYPoints = polygon.getYPoints();
        int lastIndex = polygon.size() - 1;
        int i = 0;
        do {
            cutsVertex = DoVectorsIntersect(new Vector(randomPoint, point), new Vector(new Point(polygonXPoints[lastIndex], polygonYPoints[lastIndex]), new Point(polygonXPoints[i], polygonYPoints[i])));
            lastIndex = i;
            i++;
        } while (i < polygon.size() && !cutsVertex);
        return cutsVertex;
    }


}
