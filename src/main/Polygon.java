package main;

/**
 * Created by tobias on 11.02.14.
 */
public class Polygon {
    public static final Integer X=0;
    public static final Integer Y=1;
    private double[][] points;
    private int size;

    public Polygon(double[] pointsX, double[] pointsY){

        if (pointsX != null && pointsY != null && pointsX.length == pointsY.length){
            points = new double[2][pointsX.length];
            size = pointsX.length;
            for (int i = 0; i < pointsX.length; i++){
                points[X][i] = pointsX[i];
                points[Y][i] = pointsY[i];
            }
        }else{
            throw new IllegalArgumentException("Fuck you! You gave me the wrong args");
        }
    }

    public void addPoint(int index, double x, double y){
        double[][] newPoints = new double[points[X].length + 1][points[Y].length + 1];
        boolean inserted = false;
        for (int i = 0; i < newPoints[X].length; i++){
            if (i == index) {
                newPoints[X][i] = x;
                newPoints[Y][i] = y;
                inserted = true;
            }
            if (!inserted) {
                newPoints[X][i] = x;
                newPoints[Y][i] = y;
            } else {
                newPoints[X][i + 1] = x;
                newPoints[Y][i + 1] = y;
            }
        }
        points = newPoints;
        size++;
    }

    public void Translate(int direction, double value){
        if (direction != X && direction != Y)
            return;

        for(int i = 0; i < points[direction].length; i++){
            points[direction][i] += value;
        }
    }

    public int size(){
       return size;
    }

    public double[] getXPoints(){
        return points[X];
    }

    public double[] getYPoints(){
        return points[Y];
    }

    public double[][] getPoints(){
        return points;
    }
}
