package main;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by tobias on 11.02.14.
 */
public class Model {

    private Controller controller;
    private LinkedList<Polygon> polygons;
    private Polygon ship;
    private Polygon winPolygon;
    private double shipSpeed;

    public Model(Controller controller){
        this.controller = controller;
        this.polygons = new LinkedList<Polygon>();
    }

    public void createShip(double shipSpeed, Polygon ship, int startX, int startY){
        this.ship = ship;
        this.shipSpeed = shipSpeed;
        ship.Translate(Polygon.X,startX);
        ship.Translate(Polygon.Y,startY);
        winPolygon = new Polygon(new double[]{0,controller.getWidth()},new double[]{controller.getHeight() + Utility.max(ship.getYPoints()),controller.getHeight() + Utility.max(ship.getYPoints())});
    }

    public void addPolygon(Polygon p){
        polygons.add(p);
    }

    public Polygon[] getPolygons(){
        Polygon[] p = new Polygon[polygons.size()];
        ((List)polygons).toArray(p);
        return p;
    }

    public Polygon getShip(){
        return ship;
    }

    public void update(){
        ship.Translate(Polygon.Y,shipSpeed*0.25);
    }

    public void moveShip(int direction){
        if (direction > 0){
            ship.Translate(Polygon.X, shipSpeed);
        } else {
            ship.Translate(Polygon.X, -shipSpeed);
        }
    }

    public boolean collisionDetected(){
        System.out.println("collision detecting");
        for (int i = 0; i < polygons.size(); i++){
            if (Polygons.DoPolygonsIntersect(ship,polygons.get(i))){
                System.out.println("COLLISION");
                return true;
            }
        }
        return false;
    }

    public boolean gameWon(){
        System.out.println("check won");
        if (ship == null || winPolygon == null)
            return false;

        return Polygons.DoPolygonsIntersect(ship, winPolygon);
    }
}
