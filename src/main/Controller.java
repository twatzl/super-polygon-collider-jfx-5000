package main;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.animation.TimelineBuilder;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Controller {

    @FXML // fx:id="canvas"
    private Canvas canvas; // Value injected by FXMLLoader

    @FXML
    private GridPane pane;

    @FXML
    void onKeyPressed(ActionEvent event) {
    }

    private View view;
    private Model model;
    private Timeline game;

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    private double height;
    private double width;

    public Controller(){

    }

    public void start(Stage primaryStage) {
        model = new Model(this);
        view = new View(this, model);

        canvas.setWidth(getWidth());
        canvas.setHeight(getHeight());

        model.addPolygon(new Polygon(new double[]{0,400,0},new double[]{0,250,400}));
        model.addPolygon(new Polygon(new double[]{0,200,0},new double[]{0,200,800}));
        model.addPolygon(new Polygon(new double[]{200,600,600},new double[]{0,0,300}));
        model.addPolygon(new Polygon(new double[]{600,300,600},new double[]{200,500,700}));
        model.addPolygon(new Polygon(new double[]{0,0,450},new double[]{800,400,800}));

        primaryStage.getScene().addEventHandler(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                switch (keyEvent.getCode()) {
                    case W:
                        break;
                    case A:
                    case LEFT:
                        model.moveShip(-1);
                        break;
                    case D:
                    case RIGHT:
                        model.moveShip(1);
                        break;
                }
            }
        });

        restart();
    }

    public void restart(){
        if (model == null || view == null)
            return;

        model.createShip( 1.5, new Polygon(new double[]{0,100,50}, new double[]{0,0,50}), 150, 50);

        final Duration oneFrameAmt = Duration.millis(1000/60);
        final KeyFrame oneFrame = new KeyFrame(oneFrameAmt,
                new EventHandler<ActionEvent>() {

                    @Override
                    public void handle(ActionEvent event) {
                        model.update();
                        view.Update(getCanvas());

                        if (model.collisionDetected()){
                            view.lost();
                            if (game != null) {
                                game.stop();
                            }

                        }

                        if (model.gameWon()){
                            view.won();
                            if (game != null) {
                                game.stop();
                            }
                        }


                    }
                }); // oneFrame

        // sets the game world's game loop (Timeline)
        game = TimelineBuilder.create()
                .cycleCount(Animation.INDEFINITE)
                .keyFrames(oneFrame)
                .build();
        game.play();
    }

    public Canvas getCanvas(){return canvas;}


    private void stop(){
        game.stop();
    }
}
