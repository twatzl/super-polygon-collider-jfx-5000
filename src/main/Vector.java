package main;

/**
 * Created by tobias on 12.02.14.
 */
public class Vector {

    public Point getStart() {
        return start;
    }

    public void setStart(Point start) {
        this.start = start;
    }

    public Point getEnd() {
        return end;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    private Point start;
    private Point end;

    public Vector (Point start, Point end){
        this.start = start;
        this.end = end;
    }

    public double getDeltaX(){
        return getEnd().getX() - getStart().getX();
    }

    public double getDeltaY(){
        return getEnd().getY() - getStart().getY();
    }
}
