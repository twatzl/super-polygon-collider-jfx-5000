package main;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader(getClass().getResource("main.fxml"));
        Controller controller = new Controller();
        loader.setController(controller);
        Parent root = (Parent) loader.load();
        controller.setHeight(800);
        controller.setWidth(600);
        primaryStage.setTitle("Super Polygon Collider JFX 5000");
        primaryStage.setMaxHeight(controller.getHeight());
        primaryStage.setMinHeight(controller.getHeight());
        primaryStage.setMaxWidth(controller.getWidth());
        primaryStage.setMinWidth(controller.getWidth());
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();

        controller.start(primaryStage);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
