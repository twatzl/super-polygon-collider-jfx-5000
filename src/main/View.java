package main;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;

/**
 * Created by tobias on 11.02.14.
 */
public class View {

    private Controller controller;
    private Model model;
    private Canvas canvas;

    public View(Controller controller, Model model){
        this.controller = controller;
        this.model = model;
    }

    public void lost(){
        Button button = new Button("Neustarten");
        final Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        Scene scene = new Scene(new Group(new Text(0,40,"Verloren!"),button));
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controller.restart();
                dialog.close();
            }
        });
        dialog.setScene(scene);
        dialog.show();
    }

    public void won(){
        Button button = new Button("Neustarten");
        final Stage dialog = new Stage();
        dialog.initStyle(StageStyle.UTILITY);
        Scene scene = new Scene(new Group(new Text(0,40,"Gewonnen!"),button));
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                controller.restart();
                dialog.close();
            }
        });
        dialog.setScene(scene);
        dialog.show();
    }

    public void Update(Canvas canvas){
        draw(canvas);
    }

    private void draw(Canvas canvas) {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        Polygon[] polygons = model.getPolygons();

        gc.clearRect(0,0,controller.getWidth(),controller.getHeight());

        for (Polygon p : polygons){
            gc.fillPolygon(p.getXPoints(),p.getYPoints(),p.size());
        }

        gc.strokePolygon(model.getShip().getXPoints(),model.getShip().getYPoints(),model.getShip().size());

        /*gc.setFill(Color.ALICEBLUE);
        gc.fillText("hello_world",0,0);
        gc.fillText("100",100,100);
        gc.setFill(Color.BLACK);
        gc.fillOval(100,100,100,100);
        gc.fillPolygon(new double[]{0,300,300},new double[]{0,300,0},3);/*
        gc.fill();*/

    }
}
